from utils import get_wordnet_pos
from nltk import pos_tag
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize, sent_tokenize
from nltk.stem import WordNetLemmatizer
import spacy


class CountBasedSummarizer(object):

    def __init__(self):
        self.stopwords = set(stopwords.words('english'))
        self.lemmatizer = WordNetLemmatizer()

    def lemmatize_sent(self, sentence):
        tagged_sentence = pos_tag(sentence)
        lemmatized_sentence = []
        for token, ptag in tagged_sentence:
            wordnet_ptag = get_wordnet_pos(ptag)
            lemmatized_token = self.lemmatizer.lemmatize(token, wordnet_ptag)
            lemmatized_sentence.append(lemmatized_token)
        return lemmatized_sentence

    def lemmatize_document(self, document):
        lemmatized_document = []
        tokenized_document = [word_tokenize(sent) for sent in document]
        for sentence in tokenized_document:
            lemmatized_sentence = self.lemmatize_sent(sentence)
            lemmatized_document.append(lemmatized_sentence[:])
        return lemmatized_document

    def get_word_scores(self, lemmatized_document):
        word_scores = {}
        for sentence in lemmatized_document:
            for token in sentence:
                if token in self.stopwords: continue
                if token not in word_scores: word_scores[token] = 0
                word_scores[token] += 1
        return word_scores

    def score_sent(self, sentence, word_scores):
        sentence = word_tokenize(sentence)
        sentence = self.lemmatize_sent(sentence)
        return sum([word_scores[token] for token in sentence if token in word_scores])

    def extract_sentences(self, document, topn=5):
        sentences = sent_tokenize(document)
        lemmatized_sentences = self.lemmatize_document(sentences)
        word_scores = self.get_word_scores(lemmatized_sentences)
        ranked_sentences = [(sentid, self.score_sent(lsent, word_scores))
                            for sentid, lsent in enumerate(sentences)]

        ranked_sentences = sorted(ranked_sentences, key=lambda t: t[1], reverse=True)[:topn]
        ranked_sentences = sorted(ranked_sentences, key=lambda t: t[0])
        return [sentences[sentid] for sentid, score in ranked_sentences]


class NERSummarizer(CountBasedSummarizer):

    def __init__(self):
        super(NERSummarizer, self).__init__()
        self.nlp = spacy.load('en_core_web_sm')
        self.entity_score = 2

    def score_sent(self, sentence, word_scores):
        token_score = super(NERSummarizer, self).score_sent(sentence, word_scores)
        spacy_document = self.nlp(sentence)
        return token_score + len(spacy_document.ents)*self.entity_score
