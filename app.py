from flask import Flask, render_template, request, flash
from wtforms import Form, validators, TextField
from flask_bootstrap import Bootstrap
from site_parser import document_from_url
from doc_scorers import CountBasedSummarizer

app = Flask(__name__)
Bootstrap(app)
summarizer = CountBasedSummarizer()


class ReusableForm(Form):
    url = TextField('URL:', validators=[validators.required()])
    

@app.route('/', methods=['POST', 'GET'])
def index():
    form = ReusableForm(request.form)
    summered_news = None
    title = None
    date = None
    if request.method == 'POST':
        url = request.form['url']
        if form.validate():
            title, document, date = document_from_url(url)
            summered_news = summarizer.extract_sentences(document, topn=10)
            summered_news = '\n'.join(summered_news)
        else: flash('All the form fields are required. ')
    return render_template('index.html', form=form, summered_news=summered_news, title=title, date=date)


if __name__ == '__main__':
    app.run()