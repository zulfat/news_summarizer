from newspaper import Article

def document_from_url(url):
    article = Article(url)
    article.download()
    article.parse()
    title, document, dtime = article.title, article.text, article.publish_date
    dtime = dtime.strftime("%B %d, %Y") if dtime else 'Unknown'
    return title, document, dtime